# PROJETO DE DESENVOLVIMENTO DO BANCO DE DADOS
O projeto foi desenvolvido durante a disciplina de Banco de Dados 2, como uma APS (Atividade Prática Supervisionada).  

**INSTITUIÇÃO:** Universidade Tecnológica Federal do Paraná (UTFPR), Câmpus de Pato Branco

**CURSO:** Engenharia de Computação

**PERÍODO:** 7º

**DISCIPLINA:** Banco de Dados 2 (BD27CP)

**PROFESSOR:** Ives Pola

**ALUNOS:** 

* Diogo Filipe Micali Mota 

* Gabriel Henrique Meurer

* Mário Alexandre Rodrigues

## 1. Introdução
O conceito do SIGEEC gira em torno de gerenciar o estoque de produtos e utilizar de *tags RFID*, que servirão como uma comanda que contém os produtos que o cliente deseja comprar. Portanto, o banco de dados foi pensado para armazenar: 
* Dados de diversos produtos, como código do produto, descrição, valor, quantidade, entre outros;
* Dados referentes às *tags RFID*, como seu código, data de criação e se está sendo utilizada no momento;
* Dados referentes às compras dos clientes, indicando qual *tag* foi utilizada, quais produtos foram comprados, a data da compra, entre outros.

Todo o projeto foi feito utilizando o SGBD [PostgreSQL](https://www.postgresql.org) na versão 13.0, com o auxílio da ferramenta [pgAdmin](https://www.pgadmin.org) na versão 4.25.

## 2. Diagrama Entidade-Relacionamento
Com base nos objetivos iniciais, foi desenvolvido o seguinte diagrama Entidade-Relacionamento. 

![Diagrama Entidade-Relacionamento](database/images/entity-relation_diagram.png)

Com esse diagrama, é possível abstrair e identificar os pontos principais do banco de dados em questão, obtendo uma base para entender como tudo irá funcionar.

A tabela abaixo explica melhor cada entidade e relacionamento, bem como o fluxo de trabalho e a organização dentro do banco.

| Objeto                  | Tipo                 | Descrição |
| ----------------------- | -------------------- | --------- |
| *data_analysis*         | Esquema              | Contém todas as tabelas usadas para análise de dados. Tem como objetivo isolar essas tabelas para facilitar a organização do banco. |
| *logs*                  | Esquema              | Contém todas as tabelas usadas para auditoria de dados. Tem como objetivo isolar essas tabelas para facilitar a organização do banco. |
| *public*                | Esquema              | Contém todas as tabelas referentes ao SIGEEC em si. Tem como objetivo isolar essas tabelas para facilitar a organização do banco. Pode-se considerar o esquema principal. |
| *public.product*        | Entidade             | Armazena os dados dos produtos, ou seja, funciona basicamente como um estoque. |
| *public.tag*            | Entidade             | Armazena os dados das *tags RFID*. |
| *public.sale*           | Entidade Associativa | Foi pensada para armazenar temporariamente os dados das vendas que estão em aberto, podendo ser finalizadas, alteradas ou canceladas a qualquer momento. |
| *public.ticket*         | Entidade             | Armazena os dados apenas das vendas que foram finalizadas. |
| *data_analysis.invoice* | Entidade             | Armazena temporariamente os dados das vendas executadas em um determinado intervalo de tempo para que seja feita uma eventual análise desses dados. |
| *logs.product*          | Entidade             | Armazena todas as transações executadas na entidade *public.product*, junto com os usuários que as executaram e a data da execução. |
| *logs.ticket*           | Entidade             | Armazena todas as transações executadas na entidade *public.ticket*, junto com os usuários que as executaram e a data da execução. |
| *fill_invoice*          | Relação              | Conecta as entidades *public.product* e *public.ticket* à entidade *data_analysis.invoice*. No banco é representada como uma função que popula a tabela invoice para que possa ser feita a análise dos dados. |
| *audit*                 | Relação              | Pode ser considerada como uma abstração, que conecta as entidades no esquema *public* às suas correspondentes em  *logs*. |

## 3. Mapeamento e Modelagem
Com base no diagrama anterior, o modelo lógico a seguir foi desenvolvido. Nele, é possível perceber como as entidades, agora tabelas, se interligam e quais atributos as conectam.

![Modelo Lógico](database/images/logical_model_diagram.png)

As tabelas a seguir explicam textualmente a finalidade de cada atributo.

**_public.product_** 

| Atributo      | Descrição |
| ------------- | --------- |
| *id*          | Código único para cada produto. |
| *description* | Descrição do produto. |
| *amount*      | Estrutura que armazena tanto a quantidade de produtos em estoque, quanto a unidade de medida desses produtos. |
| *price*       | Valor unitário de cada produto. |
| *fab_date*    | Data de fabricação do produto. |
| *exp_date*    | Data de validade do produto. |
| *provider*    | Fornecedor ou fabricante do produto. |

**_public.tag_**  

| Atributo   | Descrição |
| ---------- | --------- |
| *id*       | Código único para cada *tag RFID*. |
| *creation* | Data de criação *tag* no banco. |
| *in_use*   | Indica se a *tag* está em uso ou não no momento. |

**_public.sale_** 

| Atributo     | Descrição |
| ------------ | --------- |
| *tag_id*     | Código da *tag* que está armazenando a venda em questão. |
| *product_id* | Código do produto que está sendo vendido no momento. |
| *ticket_id*  | Código do *ticket* vinculado à venda atual. |
| *amount*     | quantidade do produto em questão. |
| *sub_total*  | quantidade do produto multiplicado pelo valor unitário do mesmo.|

**_public.ticket_** 

| Atributo    | Descrição |
| ----------- | --------- |
| *id*        | Código único para cada *ticket*. |
| *date_in*   | Data de início da venda. |
| *date_out*  | Data de finalização da venda. |
| *details*   | Estrutura que armazena a os dados referentes aos produtos após a venda ser finalizada. |

**_data_analysis.invoice_** 

| Atributo     | Descrição |
| ------------ | --------- |
| *ticket_id*  | Código do *ticket* vinculado à venda já finalizada. |
| *product_id* | Código do produto que foi vendido. |
| *tag_id*     | Código da *tag* que armazenou a venda. |
| *amount*     | quantidade do produto em questão. |
| *sub_total*  | quantidade do produto multiplicado pelo valor unitário do mesmo.|

**_logs.product_** 

| Atributo      | Descrição |
| ------------- | --------- |
| *exec_proc*   | Indica qual transação foi realizada. |
| *exec_date*   | Data de execução. |
| *exec_user*   | Usuário que executou a transação. |
| *id*          | Código único para cada produto. |
| *description* | Descrição do produto. |
| *amount*      | Estrutura que armazena tanto a quantidade de produtos em estoque, quanto a unidade de medida desses produtos. |
| *price*       | Valor unitário de cada produto. |
| *fab_date*    | Data de fabricação do produto. |
| *exp_date*    | Data de validade do produto. |
| *provider*    | Fornecedor ou fabricante do produto. | 

**_logs.ticket_** 

| Atributo    | Descrição |
| ----------- | --------- |
| *exec_proc* | Indica qual transação foi realizada. |
| *exec_date* | Data de execução. |
| *exec_user* | Usuário que executou a transação. |
| *id*        | Código único para cada *ticket*. |
| *date_in*   | Data de início da venda. |
| *date_out*  | Data de finalização da venda. |
| *details*   | Estrutura que armazena os dados referentes aos produtos vendidos. |

### 3.1. Análise de integridade
#### 3.1.1 - Integridade Física
Integridade física se refere a como o banco de dados lida com situações em que o armazenamento dos dados se encontra em risco, como em caso de desastres naturais, quedas de energia, atividade de hackers ou hardware comprometido. É possível a utilização de vários métodos para garantir essa integridade. No entanto, o banco de dados em questão ainda não utiliza alguns desses métodos, como servidores de *backup* ou sistema de *firewall*, apenas métodos de verificação de dados inseridos pelos usuários, que também se encaixam no conceito de integridade lógica. 

#### 3.1.2 - Integridade Lógica
Integridade lógica busca tratar da sanidade dos dados, assegurando que os mesmos façam sentido no contexto em que estão inseridos. Apesar da integridade lógica também tratar de algumas das mesmas adversidades da integridade física, os tipos de tratamento aplicados são diferentes. Além disso, a integridade lógica pode ser divida em quatro tipos de integridade, sendo elas integridade da entidade, referencial, de domínio e definida pelo usuário.

##### Integridade da entidade
A integridade da entidade diz que toda tabela deve ter pelo menos uma chave primária única e não nula. O banco de dados em pauta adere à integridade de entidade visto que todas suas chaves primárias são únicas e não nulas, sendo também numeradas e referentes à uma identidade previamente assinalada as tuplas da tabela.

##### Integridade referencial
A integridade referencial lida com a chave estrangeira de uma tabela, essa chave deve referenciar a chave primária de outra tabela ou, em alguns casos, pode ser nula. Esse tipo de integridade também está aplicada nesse banco de dados. Chaves estrangeiras são utilizadas quando é necessário referenciar a chave primária de outra tabela, garantindo a integridade referencial dos dados e a correlação necessária entre as tabelas.

##### Integridade de domínio
Os atributos de uma tabela em um banco de dados pertencem a um domínio, uma faixa de valores que são válidos para esse atributo. A integridade de domínio lida com as regras que são aplicadas à inserção de dados, limitando e formatando os dados das tabelas. O banco de dados em questão garante essa formatação e limitação de dados, sempre fazendo a checagem durante a inserção, remoção e atualização dos dados da tabela, de modo que o mesmo sempre esteja funcionando, independente de erros humanos.

##### Integridade definida pelo usuário
Integridade definida pelo usuário é quando o usuário do banco decide definir alguma regra para a utilização do mesmo para que o banco atenda as suas necessidades particulares cuja definição não se encaixa nos outros tipos de integridade. Esse tipo de integridade ainda não foi aplica no banco de dados em pauta, visto que os usuários do banco até agora são os próprios criadores, logo as regras estabelecidas até agora estão enraizadas no código.

### 3.2. Criação de Tabelas
Antes de utilizar o script, é necessário criar o banco de dados utilizando 
~~~slq
create database sigeec_db;
~~~ 
É necessário também alterar o banco de dados para que os índices criados (seção 4) sejam priorizados nas buscas. Para tal, utilza-se 
~~~sql
set enable_seqscan = off;
~~~
O script para a criação das tabelas, bem como todas as outras estruturas que serão apresentadas nas próximas seções se encontra no arquivo [*sigeec_db.sql*](database/sigeec_db.sql).

### 3.3. Populando as tabelas
Os dados utilizados para povoar as tabelas *public.product* e *public.tag* foram gerados utilizando a ferramenta [mockaroo](https://www.mockaroo.com). Esses dados foram utilizados apenas para fim de testes, sendo bem efetivos para comprovar as funcionalidades do banco de dados.

Antes de povoar as tabelas, é necessário alterar o *datestyle* do banco para *ISO, MDY*. Para isso, deve-se:
1. executar a *query*:
~~~sql
alter database sigeec_db set datestyle to ISO, MDY;
~~~
2. reiniciar o banco de dados.

> Alterar o *datestyle* do banco de dados é necessário apenas por que os dados gerados estão com as datas nesse formato. Caso as tabelas sejam povoadas com outros dados, dependendo da forma como as datas estão sendo inseridas, esse passo pode ser ignorado.

O script que popula a tabela *public.product* se encontra no arquivo [*product.sql*](database/test/product.sql).

O script que popula a tabela *public.tag* se encontra no arquivo [*tag.sql*](database/test/tag.sql).

Para popular a tabela *public.sale*, utiliza-se 
~~~sql
select fill_sale(_limit integer);
~~~
substituindo o parâmetro por um valor adequado.
A função *fill_sale* será explicada na seção 5.4.

Para popular a tabela *public.ticket*, utiliza-se 
~~~sql
select confirm_sale(ticket_id_reference bigint);
~~~
substituindo o parâmetro por um valor adequado.
A função *confirm_sale* será explicada seção 5.5.

Para popular a tabela *data_analysis.invoice*, utiliza-se 
~~~sql
select fill_invoice(start_date timestamp, end_date timestamp); 
~~~
substituindo os parâmetros por valores adequados.
A função *fill_invoice* será explicada seção 5.8.

As tabelas logs.product e logs.ticket são preenchidas automaticamente pelos *triggers* *trigger_plp* (Seção 8.6) e *trigger_tlp* (Seção 8.7), respectivamente.

## 4. Índices
Os índices têm como objetivo acelerar consultas corriqueiras no banco de dados. Pensando nisso, foram criados alguns índices para acelerar tanto consultas feitas manualmente, quanto consultas feitas automaticamente pelos *triggers* e funções, tendo um foco maior nesse último pois são feitos com maior frequência dentro do banco de dados.
A maioria dos índices foram criados sobre a tabela *public.product*. Isso se justifica considerando que a tabela em questão foi criada para armazenar milhares de itens e, sendo acessada frequentemente, algumas consultas podem ser muito lentas.

### 4.1. idxExpired
Utiliza a data de validade do produto (*exp_date*) como parâmetro. O mesmo torna-se muito útil em consultas que verifiquem essa data, como por exemplo, uma que permita verificar quais itens já passaram do prazo de validade, ou uma que verifique quais itens expiram em um determinado intervalo de tempo, para que possa ser feito a renovação do estoque.

Exemplo de consulta que utiliza esse índice:
~~~sql
-- Essa consulta verifica se existe algum produto em estoque que 
-- tenha passado da data de validade no momento da consulta.
explain analyze
select id, description, exp_date 
from product
where exp_date <= (now())::timestamp;
~~~

### 4.2. idxLacking
Utiliza a quantidade de produtos em estoque (*amount->>’quantity’*) como parâmetro. O objetivo desse índice é acelerar as consultas que buscam itens em falta no estoque, como determinado na cláusula *where*. Foi determinada a quantidade limite de itens como 10 após considerar a quantidade média de produtos em estoque. 

Exemplo de consulta que utiliza esse índice:
~~~sql
-- Essa consulta verifica quais produtos em estoque tem quantidade menor que 9, 
-- um valor qualquer que é englobado pelo índice
explain analyze
select id, description, amount->>'quantity' from product
where (amount->>'quantity')::real < 9;
~~~

### 4.3. idxSale
Utiliza as chaves estrangeiras da tabela *public.sale* (*tag_id*, *product_id*, *ticket_id*) como parâmetro. Como a tabela *public.sale* é uma das tabelas principais do banco de dados, são realizadas muitas consultas sobre ela por funções (seção 5) e *triggers* (seção 8). Dito isso, indexar essas chaves estrangeiras torna-se essencial.

Exemplo de consulta que utiliza esse índice:
~~~sql
-- Essa consulta é a mesma executada na função detailed_sale().
-- Foi pensada para retornar a venda, que ainda está em aberto, 
-- de forma detalhada dado o código da tag como parâmetro.
explain analyze
select
	public.product.id,
	public.product.description,
	public.sale.amount,
	public.sale.sub_total
from public.sale, public.product
where public.sale.product_id = public.product.id and public.sale.tag_id = 1
order by public.product.id;
~~~

> Esse índice também é utilizado pela função *detailed_sale*, como é visto na seção 5.7.

### 4.3. idxInvoice
Utiliza as chaves estrangeiras da tabela *data_analysis.invoice* (*ticket_id*, *product_id*) como parâmetro. Diminui o tempo para consultas relacionadas à análise dos dados, como na visão materializada *best_sellers*, que retorna os produtos mais vendidos dentro de um intervalo de tempo.

Exemplo de consulta que utiliza esse índice:
~~~sql
-- Essa consulta é a mesma executada na visão materializada best_sellers.
explain analyze
select 
	data_analysis.invoice.product_id,
	public.product.description,
	sum(data_analysis.invoice.amount),
	(sum(data_analysis.invoice.amount) * public.product.price)
from public.product, data_analysis.invoice
where public.product.id = data_analysis.invoice.product_id
group by data_analysis.invoice.product_id, public.product.description, public.product.price
order by sum(data_analysis.invoice.amount) desc;
~~~

## 5. Funções
As funções foram pensadas para resolver problemas encontrados durante a implementação do banco de dados. 

### 5.1. update_info(info jsonb, _key text, _value text) returns jsonb
Altera os valores de um objeto *jsonb*. É utilizada principalmente para atualizar o valor de *amount->>'quantity'* na tabela *public.product*. 

**Parâmetros de Entrada:**
* ***info jsonb*:** Objeto o qual terá o valor alterado;
* ***_key text*:** Chave que terá seu respectivo valor alterado;
* ***_value text*:** Novo valor para a chave *_key*.

**Saída:**
* ***jsonb*:** Objeto *info* atualizado com o novo valor.

Exemplo de uso dessa função:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
update public.product set amount = update_info(amount, 'quantity', ([REAL])::text)
where public.product.id = [BIGINT];
~~~

### 5.2. count_products_amount(details jsonb) returns real
Soma a quantidade total de produtos em um objeto *jsonb*. Foi criada pensando na estrutura do atributo *details* em
*public.ticket*, portanto não funcionará para objetos *jsonb* estruturados de forma diferente.

**Parâmetros de Entrada:**
* ***details jsonb*:** Objeto que terá a quantidade total de produtos somada.

**Saída:**
* ***real*:** Quantidade total de produtos em uma venda.

Exemplo de uso dessa função:
~~~sql
select 
	count_products_amount(details),
from public.ticket
~~~

### 5.3. count_products_total(details jsonb) returns real
Soma o valor total de produtos em um objeto *jsonb*. Foi criada pensando na estrutura do atributo *details* em
*public.ticket*, portanto não funcionará para objetos *jsonb* estruturados de forma diferente.

**Parâmetros de Entrada:**
* ***details jsonb*:** Objeto que terá o valor total de produtos somado.

**Saída:**
* ***real*:** Valor total de produtos em uma venda.

Exemplo de uso dessa função:
~~~sql
select 
	count_products_total(details),
from public.ticket
~~~

### 5.4. fill_sale(_limit integer) returns boolean
Gera uma quantidade x de linhas na tabela *public.sale* com valores aleatórios. Foi pensada para testar as funcionalidades relacionadas à tabela.

**Parâmetros de Entrada:**
* ***_limit integer*:** Quantidade de linhas que serão preenchidas na tabela.

**Saída:**
* ***boolean*:** Retorna *true* se a função executar corretamente, e *false* caso ocorra alguma exceção.

Exemplo de uso dessa função:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
select fill_sale([INTEGER]);
~~~

### 5.5. confirm_sale(tag_id_reference bigint) returns boolean
Finaliza uma venda que está em andamento. Devido ao *trigger_sdp* (seção 8.3), a tabela *public.ticket* é preenchida devidamente com os dados dessa venda. Portanto, cabe à função apenas apagar as linhas da tabela *public.sale*,

**Parâmetros de Entrada:**
* ***tag_id_reference bigint*:** Código da *tag* referente a venda que deseja finalizar.

**Saída:**
* ***boolean*:** Retorna *true* se a função executar corretamente, e *false* caso ocorra alguma exceção.

Exemplo de uso dessa função:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
select confirm_sale([BIGINT]);
~~~

### 5.6. cancel_sale(tag_id_reference bigint) returns boolean
Cancela uma venda que está em andamento. Devido ao *trigger_sdp* (seção 8.3), a tabela *public.ticket* é preenchida devidamente com os dados dessa venda. Portanto, cabe à função apagar as linhas da tabela *public.sale* e apagar as linhas que possuem o mesmo código do *ticket* na tabela *public.ticket*. Graças ao *trigger_tdp* (seção 8.4), o estoque de produtos é reposto.

**Parâmetros de Entrada:**
* ***tag_id_reference bigint*:** Código da *tag* referente a venda que deseja cancelar.

**Saída:**
* ***boolean*:** Retorna *true* se a função executar corretamente, e *false* caso ocorra alguma exceção.

Exemplo de uso dessa função:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
select cancel_sale([BIGINT]);
~~~

### 5.7. detailed_sale(tag_id_reference bigint) returns boolean
Exibe os detalhes da venda que está em aberto dado o código da *tag*.

**Parâmetros de Entrada:**
* ***tag_id_reference bigint*:** Código da *tag* referente a venda que se deseja ver os detalhes.

**Saída:**
* ***boolean*:** Retorna *true* se a função executar corretamente, e *false* caso ocorra alguma exceção.

Exemplo de uso dessa função:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
select * from detailed_sale([BIGINT]);
~~~


### 5.8. fill_invoice(start_date timestamp, end_date timestamp) returns boolean
Preenche a tabela *data_analysis.invoice* com os dados das vendas contidas em *public.ticket* baseado no intervalo de tempo solicitado. Essa função apenas preenche a tabela e atualiza as visões materializadas vinculadas, ficando a cargo de outras funções fazer a análise de dados em si.

**Parâmetros de Entrada:**
* ***start_date*:** Data de início do intervalo.
* ***end_date*:** Data de fim do intervalo.

**Saída:**
* ***boolean*:** Retorna *true* se a função executar corretamente, e *false* caso ocorra alguma exceção.

Exemplo de uso dessa função:
~~~sql
-- Para preencher a tabela com dados de até 1 dia atrás
select fill_invoice((now() - interval '1 DAY')::timestamp, now()::timestamp);
~~~

## 6. Visões
### 6.1. simplified_stock
Foi criada pensando em exibir apenas o necessário da tabela *public.product* para um funcionário, contendo o código do produto, a descrição, a quantidade, a unidade de medida, o valor unitário e o valor do produto multiplicado pela quantidade.
É atualizada sempre que se insere, atualiza ou remove da tabela *public.product*. 

### 6.2. sale_resume
Foi criada pensando em exibir um resumo das vendas finalizadas para um funcionário, contendo o código do *ticket*, a quantidade total de produtos vendidos, o total da venda, o tempo de duração da venda e a data.
É atualizada sempre que se insere, atualiza ou remove da tabela *public.ticket*.

### 6.3. best_sellers
Foi criada pensando em exibir quais foram os produtos mais vendidos dado um determinado intervalo de tempo, contendo o código do produto, a descrição, a quantidade de produtos vendidos e o valor do produto multiplicado pela quantidade de produtos vendidos.
É atualizada sempre que a função *fill_invoice* é chamada.

## 7. Auditoria
A auditoria é usada para poder se ter um histórico das transações executadas sobre determinadas tabelas. Dito isso, foi observado que no caso desse banco de dados não seria necessário criar auditorias para todas as tabelas, pois existem algumas que não recebem transações significativas constantemente. *public.tag* é um exemplo desse tipo de tabela. Por outro lado, *public.sale* está em constante mudança, dada a dinâmica do banco de dados. Mas essa tabela armazena dados temporariamente, portanto também não seria vantajoso criar uma auditoria para ela. O mesmo serve para *data_analysis.invoice*.

Portanto, chega-se a conclusão que as tabelas que devem ter uma auditoria relacionada são *public.product* e *public.ticket*. Olhando por um aspecto lógico, essas duas são de extrema importância para o funcionamento do banco de dados, pois uma armazena o estoque de produtos e a outra armazena um registro das vendas concretizadas.

Com relação às tabelas de auditoria, elas armazenam a cada transação sobre as tabelas vinculadas, além de todos os atributos da tabela, também dados referentes à transação: identificador da transação, data de execução e usuário que a realizou.

Manter a auditoria dessas duas tabelas pode ajudar a solucionar problemas futuros no banco de dados.

## 8. Triggers
Os *triggers* tem como objetivo realizar um procedimento quando determinada condição pré-estabelecida seja cumprida.
Com isso em mente, os *triggers* a seguir foram pensados para manter o fluxo de dados do banco alterando os valores nas devidas tabelas relacionadas. A utilização desses *triggers* também deixa o banco de dados mais conciso, uma vez que os dados são tratados da melhor forma para contornar as exceções.
As funções vinculadas a cada *trigger* não estão listadas na seção 5 pois são consideradas parte deles, sendo então detalhadas junto ao *trigger* em questão.

### 8.1. trigger_sip (Sale Insert Procedure)
O *trigger* foi pensado para agir de forma que o funcionário precise saber apenas o código do produto e a quantidade, visto que o código da *tag* será anexado à transação pelo leitor *RFID*.

Criado junto à tabela *public.sale*, é disparado sempre antes de se inserir na mesma. Executa a função **sale_insert_procedure**. Essa, por sua vez, faz as devidas verificações na tupla que será inserida, atualizando o campos *new.sub_total* e *new.ticket_id* para o valor unitário do produto multiplicado pela quantidade de produtos que se deseja inserir e para o código do *ticket* relacionado à venda, respectivamente. Com relação às demais tabelas, a função as modifica da seguinte forma:
* **_public.product_:** Atualiza a quantidade do produto em questão no estoque;
* **_public.tag_:** Informa que a *tag* está em uso, caso seja o primeiro produto inserido na venda;
* **_public.ticket_:** Cria um novo *ticket*, caso seja o primeiro produto da venda.

Exemplo de transação que dispara o *trigger*:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
insert into public.sale ([tag_id], [product_id], [amount]) 
values ([BIGINT], [BIGINT], [REAL]);
~~~

### 8.2. trigger_sup (Sale Update Procedure)
O *trigger* foi pensado para agir de forma que o funcionário necessite apenas atualizar para um novo valor a quantidade de um determinado produto em uma determinada venda vinculada a uma *tag* específica.

Criado junto à tabela *public.sale*, é disparado sempre antes de atualizar a mesma. Executa a função **sale_update_procedure**. Essa, por sua vez, faz as devidas verificações na tupla que será inserida. Com relação às demais tabelas, a função as modifica da seguinte forma:
* **_public.product_:** Atualiza a quantidade do produto em questão no estoque somando diferença da antiga quantidade pela nova.

Exemplo de transação que dispara o *trigger*:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
update public.sale set amount = [REAL or INTEGER]
where tag_id = [BIGINT] and product_id =  [BIGINT];
~~~

### 8.3. trigger_sdp (Sale Delete Procedure)
O *trigger* foi pensado para agir em dois casos: quando é necessária a remoção de um item; ou quando a venda é confirmada.

Criado junto à tabela *public.sale*, é disparado sempre após a remoção de tuplas da mesma. Executa a função **sale_delete_procedure**. Essa, por sua vez, faz as devidas verificações na(s) tupla(s) removida(s). Com relação às demais tabelas, a função as modifica da seguinte forma:
* **_public.product_:** Repõe em estoque a quantidade do produto, caso seja cancelado apenas um produto;
* **_public.ticket_**: Atualiza os detalhes da venda junto com a data de finalização no *ticket* referido.

Exemplo de transação que dispara o *trigger*:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
delete from public.sale 
where tag_id = [BIGINT] and product_id = [BIGINT];
~~~

> As funções *confirm_sale* (seção 5.5) e *cancel_sale* (seção 5.6) disparam esse trigger de maneira automatica.

### 8.4. trigger_tdp (Ticket Delete Procedure)
O *trigger* foi criado baseado no pensamento de que apenas quando se cancela uma venda é necessário remover algum valor da tabela *public.ticket*.

Criado junto à tabela *public.ticket*, é disparado sempre depois de atualizar a mesma. Executa a função **ticket_delete_procedure**. Essa, por sua vez, faz as devidas verificações na tupla que foi removida. Com relação às demais tabelas, a função as modifica da seguinte forma:
* **_public.product_:** Repõe a quantidade de cada produto associado à venda excluída.

Exemplo de transação que dispara o *trigger*:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
delete from public.ticket 
where id = [BIGINT];
~~~

> A função *cancel_sale* (seção 5.6) dispara esse trigger de maneira automatica.

### 8.5. trigger_pip (Product Insert Procedure)
O *trigger* foi pensado para controlar a integridade lógica da tabela.

Criado junto à tabela *public.product*, é disparado sempre antes de atualizar a mesma. Executa a função **product_insert_procedure**. Essa, por sua vez, faz as devidas verificações na tupla que será inserida, verificando por exemplo se a quantidade do produto é maior ou igual a zero.

Exemplo de transação que dispara o *trigger*:
~~~sql
-- Os parâmetros entre colchetes devem ser substituídos devidamente 
insert into public.product ([id], [description], [amount], [price], [fb_date], [exp_date], [provider]) 
values ([BIGINT], [VARCHAR(100)], [JSONB], [REAL], [DATE], [DATE], [VARCHAR(50)]);
~~~

### 8.6. trigger_plp (Product Log Procedure)
O *trigger* foi pensado para gerar a auditoria da tabela de modo automático.

Criado junto à tabela *public.product*, é disparado sempre após inserir, atualizar ou apagar tuplas da mesma. Executa a função **product_log_procedure**. Essa, por sua vez, além de inserir os dados da auditoria na tabela *logs.product*, também atualiza a visão *simplified_stock*.

Qualquer transação que modifique algum valor da tabela em questão, dispara o *trigger*.

### 8.7. trigger_tlp (Ticket Log Procedure)
O *trigger* foi pensado para gerar a auditoria da tabela de modo automático.

Criado junto à tabela *public.ticket*, é disparado sempre após inserir, atualizar ou apagar tuplas da mesma. Executa a função **ticket_log_procedure**. Essa, por sua vez, além de inserir os dados da auditoria na tabela *logs.ticket*, também atualiza a visão *sale_resume*.

Qualquer transação que modifique algum valor da tabela em questão, dispara o *trigger*.

## 9. Conclusão
Toda a execução do projeto foi bastante satisfatória, pois foi possível colocar em prática os conceitos aprendidos durante a disciplina. Tendo em vista que esse banco de dados poderá ser utilizado para o projeto do SIGEEC, a satisfação se torna ainda maior. 

Como dito na seção 3.1, o banco de dados ainda não tem um sistema de *backup* criado. Visando colocar o sistema em prática, é essencial analisar e implementar tanto esse, quanto outros pontos relacionados à integridade física do banco de dados.

Outro aspecto que se deve levar em consideração para o futuro é a criação de pelo menos 2 usuários para o banco, um para o usuário comum, que no caso seriam os nossos clientes, e outro para o gestor do banco de dados, ou seja, nossa empresa.

Além disso, ainda visando a implementação em um ambiente real, é necessário a adição de mais tabelas para armazenar outros dados relacionados às vendas feitas, como informações sobre o pagamento por exemplo.
