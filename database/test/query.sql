-- Tables
select * from public.sale order by tag_id;
select * from data_analysis.invoice order by ticket_id;
select * from public.product order by id;
select * from public.tag order by id;
select * from public.ticket order by id;

insert into public.sale (tag_id, product_id, amount)
values (9, 356, -15);

update public.sale set amount = 0
where tag_id = 10 and product_id = 627;

-- Audity
select * from logs.product order by id;
select * from logs.ticket order by id;

-- Materialized Views
select * from best_sellers limit 5;
select * from sale_resume;
select * from simplified_stock;

-- Functions
select fill_sale(100);
select * from detailed_sale(2);
select confirm_sale(10);
select cancel_sale(2);
select fill_invoice((now() - interval '1 MONTH')::timestamp, now()::timestamp);

-- Indexes
-- -> idxExpired
explain analyze
select id, description, exp_date 
from product
where exp_date <= (now() + interval '1 WEEK')::timestamp;
---> idxLacking
explain analyze
select id, description, amount->>'quantity' as quantity 
from product
where (amount->>'quantity')::real = 0;
---> idxSale
-- It's used in the detailed function
explain analyze
select
	public.product.id,
	public.product.description,
	public.sale.amount,
	public.sale.sub_total
from public.sale, public.product
where public.sale.product_id = public.product.id and public.sale.tag_id = 8
order by public.product.id;
---> idxInvoice
-- It's used in the materialized view best_sellers
explain analyze
select 
	data_analysis.invoice.product_id,
	public.product.description,
	sum(data_analysis.invoice.amount),
	(sum(data_analysis.invoice.amount) * public.product.price)
from public.product, data_analysis.invoice
where public.product.id = data_analysis.invoice.product_id
group by data_analysis.invoice.product_id, public.product.description, public.product.price
order by sum(data_analysis.invoice.amount) desc;

-- Função para confirmar todas as sales de uma vez
-- Serve apenas para testes
create or replace function confirm_all_sales()
returns boolean as $$
begin
	delete from public.sale;
	return true;
exception
	when others then
		return false;
end
$$ language plpgsql;

select confirm_all_sales();
