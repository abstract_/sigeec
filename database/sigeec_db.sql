-- =============================================== Schemas ================================================== --
create schema data_analysis;
create schema logs;
-- ============================================== Sequences ================================================= -- 
create sequence tag_id_sequence increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;
create sequence product_id_sequence increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;
create sequence ticket_id_sequence increment 1 minvalue 1 maxvalue 9223372036854775807 start 1 cache 1;
-- ================================================ Tables ================================================== --
-- Table Tag
create table if not exists public.tag (
	id bigint not null default nextval('tag_id_sequence'),
	creation timestamp not null,
	in_use boolean default false,

	primary key (id)
);
-- Table Product
create table if not exists public.product (
    id bigint not null default nextval('product_id_sequence') check (id > 0),
    description varchar(100),
    amount jsonb not null check ((amount->>'quantity')::real >= 0),
    price real not null check (price > 0),
    fb_date date not null,
    exp_date date not null,
    provider varchar(50),

    primary key (id)
);
-- Table Ticket
create table if not exists public.ticket (
    id bigint not null default nextval('ticket_id_sequence') check (id > 0),
    date_in timestamp not null,
	date_out timestamp,
    details jsonb,
    
    primary key (id)
);
-- Table Sale
create table if not exists public.sale (
    tag_id bigint not null,
    product_id bigint not null,
    ticket_id bigint not null,
    amount real check (amount > 0),
    sub_total real,

    foreign key(tag_id) references public.tag (id),
    foreign key(product_id) references public.product (id),
    foreign key(ticket_id) references public.ticket (id)
);
-- Table Invoice
create table if not exists data_analysis.invoice (
    ticket_id bigint not null,
    product_id bigint not null,
    tag_id bigint,
    amount real,
    sub_total real,

    foreign key(ticket_id) references  public.ticket  (id),
    foreign key(product_id) references public.product (id)
);
-- =============================================== Indexes ================================================== --
-- Indexes product quantity
create index idxLacking on public.product ((amount->>'quantity'))
where (amount->>'quantity')::real <= 10;
-- Indexes the product expiration date
create index idxExpired on public.product (exp_date);
-- Indexes the sale foreign keys
create index idxSale on public.sale (tag_id, product_id, ticket_id);
-- Indexes the invoice foreign keys
create index idxInvoice on data_analysis.invoice (ticket_id, product_id);
-- ============================================== Functions ================================================= --
-- Function to update the json data with another value
create or replace function update_info(info jsonb, _key text, _value text) 
returns jsonb as $$
	select json_object(array_agg(key), array_agg(case key when _key then _value else value end))::jsonb
	from jsonb_each_text(info);
$$ language sql;
-- Function to count the products quantity in a sale 
create or replace function count_products_amount(details jsonb) 
returns real as $$
declare
	counter real := 0;
	_key text;
	_value jsonb;
begin
	if details is not null 
	then
		for _key, _value in select * from jsonb_each(details) 
		loop
			counter := counter + (_value->>'amount')::real;	
		end loop;
	else
		counter := null;
	end if;
	return counter;
end
$$ language plpgsql;
-- Function to count the products total value in a sale 
create or replace function count_products_total(details jsonb) 
returns real as $$
declare
	counter real := 0;
	_key text;
	_value jsonb;
begin
	if details is not null then
		for _key, _value in select * from jsonb_each(details) 
		loop
			counter := counter + (_value->>'sub_total')::real;	
		end loop;
	else
		counter := null;
	end if;
	return counter;
end
$$ language plpgsql;
-- Function to fill sale table with some random values
create or replace function fill_sale(_limit integer) 
returns boolean as $$
declare
	i integer;
	product_id_tmp bigint;
	tag_id_tmp bigint;
	amount_tmp bigint; 
	quantity real;
	measure_tmp text;
	tuple record;
begin
	for i in 1.._limit loop
		select id into tag_id_tmp from public.tag order by random() limit 1;
		select id into product_id_tmp from public.product order by random() limit 1;
		amount_tmp := 1+random()*10;
		
		select * into tuple from public.sale where tag_id_tmp = public.sale.tag_id and product_id_tmp = public.sale.product_id;
		select (public.product.amount->>'quantity')::real, public.product.amount->>'measure' into quantity, measure_tmp 
		from public.product 
		where public.product.id = product_id_tmp;	
		
		while tuple is not null or quantity = 0.0 
		loop
			select id into product_id_tmp from public.product order by random() limit 1;
			select * into tuple from public.sale where tag_id_tmp = public.sale.tag_id and product_id_tmp = public.sale.product_id;
			select (public.product.amount->>'quantity')::real, public.product.amount->>'measure' into quantity, measure_tmp 
			from public.product 
			where public.product.id = product_id_tmp;		
		end loop; 
		
		if amount_tmp > quantity 
		then 
			amount_tmp := quantity; 
		end if;
		
		if measure_tmp not in ('KG', 'L') and (amount_tmp - round(amount_tmp) != 0.0)
		then
			amount_tmp := round(amount_tmp);
		end if;
		
		insert into public.sale (tag_id, product_id, amount) values (tag_id_tmp, product_id_tmp, amount_tmp);
	end loop;
	return true;
exception
	when others then
		return false;
end
$$ language plpgsql;
-- Function to finalize a sale given a tag_id
create or replace function confirm_sale(tag_id_reference bigint)
returns boolean as $$
begin
	delete from public.sale 
	where public.sale.tag_id = tag_id_reference;
	return true;
exception
	when others then
		return false;
end
$$ language plpgsql;
-- Function to cancel a sale given a tag_id
create or replace function cancel_sale(tag_id_reference bigint)
returns boolean as $$
declare
    ticket_id_tmp bigint; 
begin
	select public.sale.ticket_id into ticket_id_tmp from public.sale
	where public.sale.tag_id = tag_id_reference;
	
	delete from public.sale 
	where public.sale.tag_id = tag_id_reference;
	delete from public.ticket 
	where public.ticket.id = ticket_id_tmp;
	return true;
exception
	when others then
		return false;
end
$$ language plpgsql;
-- Function to show the sales details given a tag.
create or replace function detailed_sale(tag_id_reference bigint)
returns table (codigo bigint, produto varchar(100), quantidade real, sub_total real) as $$
begin
	return query 
	select
		public.product.id,
		public.product.description,
		public.sale.amount,
		public.sale.sub_total
	from public.sale, public.product
	where public.sale.product_id = public.product.id and public.sale.tag_id = tag_id_reference
	order by public.product.id;
end
$$ language plpgsql;
-- Function to fill the Invoice table with sales details given an interval
create or replace function fill_invoice(start_date timestamp, end_date timestamp) 
returns boolean as $$
declare
	cursor_ticket cursor for select * from public.ticket 
	where public.ticket.date_out >= start_date and  public.ticket.date_out <= end_date;
	_key text;
	_value jsonb;
begin
	delete from data_analysis.invoice;
	for tuple in cursor_ticket 
	loop
		for _key, _value in select * from jsonb_each(tuple.details) 
		loop
			if _value is not null 
			then
				insert into data_analysis.invoice (ticket_id, product_id, tag_id, amount, sub_total)
				values (tuple.id, _key::bigint, (_value->>'tag_id')::bigint, (_value->>'amount')::real, (_value->>'sub_total')::real);
			end if;
		end loop;
	end loop;
	refresh materialized view best_sellers;
	return true;
exception
	when others then
		return false;
end
$$ language plpgsql;
-- ========================================= Materialized Views ============================================= --
-- Simplified Stock
create materialized view simplified_stock (codigo, produto, estoque, unidade, preco, total) 
as select 
	id, 
	description, 
	(amount->>'quantity')::real,
	amount->>'measure',  
	price, 
	((amount->>'quantity')::real * price) 
from public.product;
-- Sale Resume
create materialized view sale_resume (codigo, quantidade, total, tempo, _data) 
as select 
	id, 
	count_products_amount(details),
	count_products_total(details),
	age(date_out, date_in), 
	date_out::date  
from public.ticket 
order by id;
-- Best Sellers
create materialized view best_sellers (codigo, produto, quantidade, total)
as select 
	data_analysis.invoice.product_id,
	public.product.description,
	sum(data_analysis.invoice.amount),
	(sum(data_analysis.invoice.amount) * public.product.price)
from public.product, data_analysis.invoice
where public.product.id = data_analysis.invoice.product_id
group by data_analysis.invoice.product_id, public.product.description, public.product.price
order by (sum(data_analysis.invoice.amount) * public.product.price) desc;
-- ============================================== Triggers ================================================== --
-- Trigger SIP (Sale Insert Procedure)
create or replace function sale_insert_procedure() 
returns trigger as $trigger_sip$
declare
    product_id_tmp bigint;
    sub_total_tmp real;
	quantity_tmp real;
    in_use_tmp boolean;
	measure_tmp text;
begin
	select public.tag.in_use into in_use_tmp from public.tag where public.tag.id = new.tag_id;
	select (new.amount*public.product.price) into sub_total_tmp from public.product 
	where public.product.id = new.product_id;

	select (amount->>'quantity')::real, (amount->>'measure')::text into quantity_tmp, measure_tmp from public.product
	where public.product.id = new.product_id;
	
	if quantity_tmp is not null 
	then
		if measure_tmp not in ('KG', 'L') and (new.amount - round(new.amount) != 0.0)
		then
			raise exception 'Measure % do not accept real values for amount', measure_tmp
			using hint = 'Use integer values only';
		end if;
		
		quantity_tmp := quantity_tmp - new.amount;
			
		if (quantity_tmp < 0) 
		then
			raise exception 'The stock does not have the requested quantity of the product %', new.product_id
			using hint = 'Please check the quantity of this product in stock';
		end if;				
	else
		raise exception 'Product % does not exist', new.product_id
		using hint = 'Please check the Product ID';
	end if;
  
	if not in_use_tmp 
	then
		new.ticket_id := nextval('ticket_id_sequence');
		new.sub_total := sub_total_tmp;

		update public.tag set in_use = true 
		where id = new.tag_id;
		
		insert into public.ticket (id, date_in) values (new.ticket_id, now());
		update public.product set amount = update_info(amount, 'quantity', (quantity_tmp)::text)
		where public.product.id = new.product_id;  
	else
		select public.sale.product_id into product_id_tmp from public.sale
		where public.sale.product_id = new.product_id and public.sale.tag_id = new.tag_id limit 1;

		if product_id_tmp is null 
		then
			select ticket_id into new.ticket_id from public.sale 
			where public.sale.tag_id = new.tag_id limit 1;
			new.sub_total := sub_total_tmp;
			update public.product set amount = update_info(amount, 'quantity', (quantity_tmp)::text)
			where public.product.id = new.product_id;  
		else
			update public.sale set amount = amount + new.amount
			where tag_id = new.tag_id and product_id = new.product_id;
			return null;
		end if;
	end if;
	return new;
end;
$trigger_sip$ language plpgsql;
create trigger trigger_sip
before insert on public.sale 
for each row execute procedure sale_insert_procedure();
-- Trigger SUP (Sale Update Procedure)
create or replace function sale_update_procedure() 
returns trigger as $trigger_sup$
declare
    product_id_tmp bigint;
    sub_total_tmp real;
	quantity_tmp real;
	measure_tmp text;
begin
	if new.amount <= 0.0 
	then
		delete from public.sale 
		where product_id = new.product_id;
		return null;
	else
		select (amount->>'quantity')::real, (amount->>'measure')::text into quantity_tmp, measure_tmp from public.product
		where public.product.id = new.product_id;
		select (new.amount*public.product.price) into sub_total_tmp from public.product 
		where public.product.id = new.product_id;

		if quantity_tmp is not null 
		then
			if measure_tmp not in ('KG', 'L') and (new.amount - round(new.amount) != 0.0)
			then
				raise exception 'Measure % do not accept real values for amount', measure_tmp
				using hint = 'Use integer values only';
			end if;

			quantity_tmp := quantity_tmp + (old.amount - new.amount);

			if (quantity_tmp < 0) 
			then
				raise exception 'The stock does not have the requested quantity of the product %', new.product_id
				using hint = 'Please check the quantity of this product in stock';
			end if;			
		else
			raise exception 'Product % does not exist', new.product_id
			using hint = 'Please check the Product ID';
		end if;

		update public.product set amount = update_info(amount, 'quantity', (quantity_tmp)::text)
		where public.product.id = new.product_id;    
		new.sub_total := sub_total_tmp;
		return new;
	end if;
end;
$trigger_sup$ language plpgsql;
create trigger trigger_sup
before update on public.sale 
for each row execute procedure sale_update_procedure();
-- Trigger SDP (Sale Delete Procedure)
create or replace function sale_delete_procedure() 
returns trigger as $trigger_sdp$
declare
	tag_id_tmp bigint;
	ticket_id_tmp bigint;
	quantity_tmp real;
	details_tmp jsonb;
begin
	select public.sale.tag_id into tag_id_tmp from public.sale
	where public.sale.tag_id = old.tag_id;

	if tag_id_tmp is null 
	then
		update public.tag set in_use = false
		where id = old.tag_id;
		select details into details_tmp from public.ticket 
		where id = old.ticket_id;
		
		if details_tmp is not null 
		then
			details_tmp := details_tmp::jsonb || (
				'{ "' || old.product_id || 
				'":{"tag_id" :' || old.tag_id || 
				',"amount" :' || old.amount ||
				',"sub_total" :' || old.sub_total || '}}'
			)::jsonb;
		else
			details_tmp := (
				'{ "' || old.product_id || 
				'":{"tag_id" :' || old.tag_id || 
				',"amount" :' || old.amount ||
				',"sub_total" :' || old.sub_total || '}}'
			)::jsonb;
		end if;
		
		update public.ticket set details = details_tmp, date_out = now()
		where id = old.ticket_id;
	else
		select (amount->>'quantity')::real into quantity_tmp from public.product
		where public.product.id = old.product_id;
		
		if quantity_tmp is not null 
		then
			quantity_tmp := quantity_tmp + (old.amount);		
		else
			raise exception 'Product % does not exist', new.product_id
			using hint = 'Please check the Product ID';
		end if;

		update public.product set amount = update_info(amount, 'quantity', (quantity_tmp)::text)
		where public.product.id = old.product_id;   
	end if;
	return null;
end;
$trigger_sdp$ language plpgsql;
create trigger trigger_sdp
after delete on public.sale 
for each row execute procedure sale_delete_procedure();
-- Trigger TDP (Ticket Delete Procedure)
create or replace function ticket_delete_procedure() 
returns trigger as $trigger_tdp$
declare
	product_id_tmp text;
	quantity_tmp real;
begin
	for product_id_tmp in select * from jsonb_object_keys(old.details) 
	loop
		select public.product.amount->>'quantity' into quantity_tmp from public.product
		where public.product.id = (product_id_tmp::bigint);
		
		if quantity_tmp is not null 
		then
			quantity_tmp := quantity_tmp + (old.details->product_id_tmp->>'amount')::real;		
		else
			raise exception 'Product % does not exist', product_id_tmp
			using hint = 'Please check the Product ID';
		end if;

		update public.product set amount = update_info(amount, 'quantity', (quantity_tmp)::text)
		where public.product.id = product_id_tmp::real;
	end loop;
	return null;
end;
$trigger_tdp$ language plpgsql;
create trigger trigger_tdp
after delete on public.ticket 
for each row execute procedure ticket_delete_procedure();
-- Trigger PIP (Product Insert Procedure)
create or replace function product_insert_procedure() 
returns trigger as $trigger_pip$
declare
	measure_tmp text;
	amount_tmp real;
begin
	amount_tmp := (new.amount->>'quantity')::real;
	measure_tmp := (new.amount->>'measure')::text;

	if amount_tmp <= 0
	then 
		raise exception 'The product quantity cannot be zero!';
	end if;

	if measure_tmp not in ('KG', 'L') and (amount_tmp - round(amount_tmp) != 0.0)
	then
		new.amount := update_info(new.amount, 'quantity', round(amount_tmp)::text);
	end if;

	return new;
end;
$trigger_pip$ language plpgsql;
create trigger trigger_pip
before insert on public.product
for each row execute procedure product_insert_procedure();
-- =============================================== Audits =================================================== --
-- Table Product Log
create table if not exists logs.product (
    exec_operation char,
    exec_date timestamp,
    exec_user varchar,
    id bigint not null,
    description varchar(100),
    amount jsonb,
    price real not null,
    fb_date date not null,
    exp_date date not null,
    provider varchar(50)
);
-- Table Ticket Log
create table if not exists logs.ticket (
    exec_operation char,
    exec_date timestamp,
    exec_user varchar,
    id bigint not null,
    date_in timestamp not null,
	date_out timestamp,
    details jsonb
);
-- Trigger PLP (Product Log Procedure)
create or replace function product_log_procedure()
returns trigger as $trigger_plp$
begin
	refresh materialized view simplified_stock;
    if (TG_OP = 'DELETE') 
    then
        insert into logs.product select 'D', now(), user, old.*;
        return old;
    elsif (TG_OP = 'UPDATE') 
    then
        insert into logs.product select 'U', now(), user, new.*;
        return new;
    elsif (TG_OP = 'INSERT') 
    then
        insert into logs.product select 'I', now(), user, new.*;
        return new;
    end if;

    return null;
end;
$trigger_plp$ language plpgsql;
create trigger trigger_plp
after insert or update or delete on public.product
for each row execute procedure product_log_procedure();
-- Trigger TLP (Ticket Log Procedure)
create or replace function ticket_log_procedure()
returns trigger as $trigger_tlp$
begin
	refresh materialized view sale_resume;
    if (TG_OP = 'DELETE') 
    then
        insert into logs.ticket select 'D', now(), user, old.*;
        return old;
    elsif (TG_OP = 'UPDATE') 
    then
        insert into logs.ticket select 'U', now(), user, new.*;
        return new;
    elsif (TG_OP = 'INSERT') 
    then
        insert into logs.ticket select 'I', now(), user, new.*;
        return new;
    end if;

    return null;
end;
$trigger_tlp$ language plpgsql;
create trigger trigger_tlp
after insert or update or delete on public.ticket
for each row execute procedure ticket_log_procedure();

-- Redefine metrics
analyze;
