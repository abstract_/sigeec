from PyQt5 import QtWidgets

class MyLineEdit(QtWidgets.QLineEdit):
  def __init__(self, parent=None):
    super(MyLineEdit, self).__init__(parent)

  def focusInEvent(self, e):
    self.selectAll()
