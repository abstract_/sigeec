from datetime import datetime
import psycopg2

class Database():
  def __init__(self, parent):
    try:
      self.connection = psycopg2.connect(host='localhost', database='sigeec_db', user='mario', password='mario')
      self.cursor = None   

    except:
      print("I am unable to connect to the database")
  
  def get_sale(self, tag_id):
    self.cursor = self.connection.cursor()
    query = """
      select
        public.sale.ticket_id,
        public.product.id,
        public.product.description,
        public.product.price,
        public.sale.amount,
        public.sale.sub_total
      from public.sale, public.product
      where public.sale.product_id = public.product.id and public.sale.tag_id = %s
      order by public.product.id;
    """
    self.cursor.execute(query, (tag_id.rstrip("\r"),))
    self.connection.commit()
    tuples = self.cursor.fetchall()
    self.cursor.close()

    return tuples
  
  def pesquisar_produtos(self, prod_id=None, desc=None):
    self.cursor = self.connection.cursor()
    query = None
    if prod_id:
      prod_id = int(prod_id)
      query = """
        select * from simplified_stock where codigo = %s
      """
      self.cursor.execute(query, (prod_id,))

    elif desc == "":
      query = """
        select * from simplified_stock
      """
      self.cursor.execute(query)

    elif desc:
      desc = '%'+str(desc)+'%'
      query = """
        select * from simplified_stock where upper(produto) like upper(%s)
      """
      self.cursor.execute(query, (desc,))
    
    tupes = None
    if query:
      self.connection.commit()
    
    tuples = self.cursor.fetchall()

    self.cursor.close()

    return tuples

  def insert_product_on_sale(self, tag_id, product_id, qtd):
    self.cursor = self.connection.cursor()
    query = """
      insert into public.sale (tag_id, product_id, amount) 
      values (%s, %s, %s)
    """
    self.cursor.execute(query, (tag_id.rstrip("\r"), product_id, float(qtd)))
    
    self.connection.commit()

  def remover_produto(self, tag_id, product_id):
    self.cursor = self.connection.cursor()
    query = """
      delete from public.sale 
        where tag_id = %s and product_id = %s;

    """
    self.cursor.execute(query, (tag_id.rstrip("\r"), product_id))
    
    self.connection.commit()

  def finalizar_venda(self, tag_id):
    self.cursor = self.connection.cursor()
    query = """
      select confirm_sale(%s);
    """
    self.cursor.execute(query, (tag_id.rstrip("\r"),))
    
    self.connection.commit()
 
  def apagar_produto(self, product_id):
    self.cursor = self.connection.cursor()
    query = """
      delete from public.product 
      where id = %s;
    """
    self.cursor.execute(query, (product_id,))
    
    self.connection.commit()

    self.cursor.close()

  def apagar_tag(self, tag_id):
    self.cursor = self.connection.cursor()
    query = """
      delete from public.tag 
      where id = %s;
    """
    self.cursor.execute(query, (tag_id,))
    
    self.connection.commit()

    self.cursor.close()

  def inserir_produto(self, produto):
    self.cursor = self.connection.cursor()
    query = """
      insert into public.product (description, amount, price, fb_date, exp_date, provider) 
      values (%s, %s, %s, %s, %s, %s);
    """
    self.cursor.execute(query, produto)
    
    self.connection.commit()
 
  def exibir_estoque(self, prod_id=None, desc=None):
    self.cursor = self.connection.cursor()
    query = None
    if prod_id:
      prod_id = int(prod_id)
      query = """
        select 
          id, 
          description, 
          (amount->>'quantity')::real,
          amount->>'measure',  
          price, 
          fab_date,
          exp_date,
          provider  
        from public.product where id = %s;
      """
      self.cursor.execute(query, (prod_id,))

    elif desc == "":
      query = """
        select 
          id, 
          description, 
          (amount->>'quantity')::real,
          amount->>'measure',  
          price, 
          fb_date,
          exp_date,
          provider  
        from public.product;
      """
      self.cursor.execute(query)

    elif desc:
      desc = '%'+str(desc)+'%'
      query = """
        select 
          id, 
          description, 
          (amount->>'quantity')::real,
          amount->>'measure',  
          price, 
          fb_date,
          exp_date,
          provider  
        from public.product where upper(description) like upper(%s)
      """
      self.cursor.execute(query, (desc,))
    
    tupes = None
    if query:
      self.connection.commit()
    
    tuples = self.cursor.fetchall()

    self.cursor.close()

    return tuples

  def atualizar_produto(self, produto):
    self.cursor = self.connection.cursor()
    query = """
      update public.product set
        description = %s, 
        amount = %s, 
        price = %s, 
        fb_date = %s, 
        exp_date = %s, 
        provider = %s
      where id = %s; 
    """
    self.cursor.execute(query, produto)
    
    self.connection.commit()

  def procurar_tags(self, tag_id=None):
    self.cursor = self.connection.cursor()
    query = None
    if tag_id == "":
      query = """
        select * from public.tag
      """
      self.cursor.execute(query)

    elif tag_id:
      tag_id = '%'+str(tag_id)+'%'
      query = """
        select * from public.tag where upper(id) like upper(%s)
      """
      self.cursor.execute(query, (tag_id,))
    
    tupes = None
    if query:
      self.connection.commit()
    
    tuples = self.cursor.fetchall()

    self.cursor.close()

    return tuples

  def adicionar_tag(self, tag_id):
    self.cursor = self.connection.cursor()
    query = """
      insert into public.tag (id, creation) 
      values (%s, %s);
    """
    self.cursor.execute(query, (tag_id.strip("\r"), datetime.now()))
    
    self.connection.commit()

  def pesquisar_tickets(self, tag_id=None, ticket_id=None, data_inicio=None, data_fim=None):
    self.cursor = self.connection.cursor()
    query = None
    if ticket_id:
        ticket_id = int(ticket_id)
        query = """
            select 
                codigo, 
                quantidade, 
                total, 
                _data
            from sale_resume where (codigo = %s) and (_data >= %s) and (_data <= %s)
            group by codigo;
        """
        self.cursor.execute(query, (ticket_id, data_inicio, data_fim))

    elif tag_id == "":
      query = """
        select 
            codigo, 
            sum(quantidade), 
            sum(total), 
            _data
        from sale_resume, public.ticket where (_data >= %s) and (_data <= %s)
        group by codigo, _data;
      """
      self.cursor.execute(query, (data_inicio, data_fim))

    elif tag_id:
      tag_id = '%'+str(tag_id)+'%'
      query = """
        select 
            codigo, 
            quantidade, 
            total, 
            _data 
        from sale_resume, public.ticket where (upper(public.ticket.details->>'tag_id') = upper(%s)) and (_data >= %s) and (_data <= %s)
        group by codigo;
      """
      self.cursor.execute(query, (tag_id, data_inicio, data_fim))
    
    tuples = None
    
    if query:
        self.connection.commit()
    try:
      tuples = self.cursor.fetchall()
    except:
      pass
    
    self.cursor.close()

    return tuples

  def apagar_ticket(self, ticket_id):
    self.cursor = self.connection.cursor()
    query = """
      delete from public.ticket 
      where id = %s;
    """
    self.cursor.execute(query, (ticket_id,))
    
    self.connection.commit()
    
    self.cursor.close()

  def cancel_sale(self, ticket_id=None):
    self.cursor = self.connection.cursor()
    query = None
    ticket_id = int(ticket_id)
    if ticket_id:
      query = """
        select cancel_sale(%s);
      """
      self.cursor.execute(query, (ticket_id,))
    print(self.cursor.fetchall())
    self.cursor.close()

  def get_invoice(self, ticket_id, data_ini, data_fim):
    self.cursor = self.connection.cursor()
    query = None
    if ticket_id:
      ticket_id = int(ticket_id)
      query = """
          select fill_invoice((%s)::timestamp, (%s)::timestamp);
      """
      self.cursor.execute(query, (data_ini, data_fim))
      
      self.connection.commit()
      
      query = """
          select 
            public.product.description,
            data_analysis.invoice.amount,
            sub_total
          from data_analysis.invoice, public.product 
          where data_analysis.invoice.product_id = public.product.id and ticket_id = %s
      """
      self.cursor.execute(query, (ticket_id,))
      
    tuples = None
    self.connection.commit()
    try:
      tuples = self.cursor.fetchall()
    except:
      pass
    
    self.cursor.close()

    return tuples
