import serial
import time
class Arduino():
  def __init__(self, parent):
    self.tag_id = ""
    self.parent = parent
    self.arduino = serial.Serial('com3', 9600)

  
  def read_tag_id(self, close_window):
    self.tag_id = ""
    time.sleep(1.8)
    self.arduino.write(b'r')
    
    self.tag_id = self.arduino.readline().decode('utf-8').rstrip("\n")
    self.parent.id_tag.setText(f"TAG {self.tag_id}")
    close_window()
  
  def get_tag_id(self):
    return self.tag_id