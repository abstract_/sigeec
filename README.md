# Proposta de projeto
## Sistema de Gerenciamento de Estoque e Comanda Eletrônica (SIGEC).

Projeto desenvolvido para a disciplina de Oficina de Integração.

**INSTITUIÇÃO:** Universidade Tecnológica Federal do Paraná (UTFPR), Câmpus de Pato Branco

**CURSO:** Engenharia de Computação

**PERÍODO:** 7º

**DISCIPLINA:** Oficina de Integração (OI28CP)

**PROFESSOR:** Gustavo Weber Denardin

**ALUNOS:** 

* Diogo Filipe Micali Mota 

* Mário Alexandre Rodrigues

## 1. Introdução
O SIGEC é um sistema que visa gerenciar o estoque de produtos de um comércio, bem como conciliar a utilização de comandas eletrônicas, por meio de *tags RFID*. O projeto tem como objetivo inicial suprir a necessidade de pequenos comerciantes que buscam mais comodidade e segurança para seus estabelecimentos. Todo o sistema foi pensado para que haja uma integração entre *hardware* (*tags RFID*) e *software* (GUI, Banco de dados).

## 2. Objetivos
Desenvolver duas interfaces, uma delas para consulta e registro de pedidos, e outra para o sistema de caixa, com cadastro de produtos, manutenção, venda e conclusão da compra;
Utilizar do *hardware* (*tags RFID*) para verificar e autenticar a comanda eletrônica;
Integrar *hardware* (Arduino e *tags RFID*) e *software* (interface gráfica e banco de dados) para execução do projeto final;
Buscar por possíveis falhas tanto no *software*, quanto na questão de segurança do sistema criado, consertando-as.

## 3. Desenvolvimento do projeto
![Diagrama representativo do SIGEC.](sigec_diagram.jpg)

### 3.1. *hardware*
O *hardware* do projeto consiste em dois computadores, dois Arduino UNO e dois Kit RFID Mfrc522 13,6MHz. Os materiais serão divididos em dois terminais independentes, um para o atendente e outro para o caixa. As funções base destes terminais envolvem o cadastro e a leitura das *tags RFID* para autenticação das compras, além do processamento das mesmas.

### 3.2. *software*
#### 3.2.1. Interface Gráfica para os Usuários
A interface gráfica será construída em Python, utilizando a biblioteca Tkinter (biblioteca gráfica padrão do interpretador Python baseada em Tcl/Tk). Como dito antes, o projeto será estruturado em dois terminais independentes, portanto será criado uma interface exclusiva para cada terminal. Na interface criada para o atendente, o foco será apenas no registro e manutenção das vendas e consulta de produtos. Já na interface para o terminal do caixa, além de todas as funcionalidades do terminal do atendente, terá todas as funcionalidades referentes ao controle de estoque como cadastro, manutenção de produtos, além da finalização de vendas e pagamento.

#### 3.2.2. Arduino
O programa que acompanhará os terminais para a leitura e cadastro das *tags RFID* será feito utilizando a linguagem de programação C. Para fins da integração do sistema, o programa será desenvolvido de modo que para cada entrada dada pelo sistema, uma saída correspondente seja devolvida.

#### 3.2.3. Banco de Dados
O banco de dados criado é responsável por armazenar as informações tanto dos produtos e das vendas, quanto das *tags RFID*. Para mais informações, visite a [página do banco de dados do projeto](https://gitlab.com/abstract_/sigeec/-/tree/master/database).

### 3.3. Integração *hardware*-*software*
A integração necessária para o funcionamento do sistema envolve tanto a conexão entre a interface dos terminais com o banco de dados quanto com o Arduino. Para conectar a interface e o banco de dados, iremos utilizar a biblioteca [Psycopg2](https://www.psycopg.org/docs/) e para a conexão entre a interface e o Arduino, utilizaremos a biblioteca [pySerial](https://pyserial.readthedocs.io/en/latest/pyserial.html) que nos permite estabelecer uma comunicação serial.

## 4. Etapas de execução
### 4.1. Desenvolvimento do *hardware* (*tags RFID*)
* Desenvolver um *hardware* que utilize *tags RFID*;
* Desenvolver o *software* utilizando a IDE do Arduino como plataforma para ler e/ou escrever os dados.

### 4.2. Desenvolvimento da Interface Gráfica
* Criar um esboço da interface;
* Escolher elementos para facilitar a experiência do usuário;
* Criação da interface utilizando a linguagem Python (Biblioteca Tkinter).

### 4.3. Integração do Sistema
* Integração da interface gráfica ao banco de dados;
* Integração da interface gráfica ao *hardware*, integrando assim todo o sistema.

### 4.4. Busca e Correção de Falhas
* Corrigir eventuais falhas do sistema como um todo;
* Buscar e corrigir possíveis erros de segurança;
* Documentar todo o processo de criação na página do gitlab.

# Copyright
Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)
