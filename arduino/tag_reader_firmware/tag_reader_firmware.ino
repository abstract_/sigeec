/*Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <EEPROM.h>     // We are going to read and write PICC's UIDs from/to EEPROM
#include <SPI.h>        // RC522 Module uses SPI protocol
#include <MFRC522.h>  // Library for Mifare RC522 Devices

#define LED_AZUL 5
#define LED_VERDE 6
#define LED_VERMELHO 7
#define RST  9
#define SDA  10
#define MOSI 11
#define MISO 12
#define SCK  13

MFRC522 mfrc522(SDA, RST);

// Definição das funções
void read_tagid();

void setup() {
  //Protocol Configuration
  Serial.begin(9600);  // Initialize serial communications with PC
  SPI.begin();           // MFRC522 Hardware uses SPI protocol
  mfrc522.PCD_Init();    // Initialize MFRC522 Hardware

  pinMode(LED_AZUL, OUTPUT);
  pinMode(LED_VERDE, OUTPUT);
  pinMode(LED_VERMELHO, OUTPUT);
  
}

void loop() {
  char protocol;  
   
  if(Serial.available() > 0) {
    protocol = Serial.read();
    if(protocol == 'r') {
      read_tagid();
    }
  }
}

void read_tagid() {
  digitalWrite(LED_AZUL, HIGH);
  while (!mfrc522.PICC_IsNewCardPresent() || !mfrc522.PICC_ReadCardSerial()) {} //VERIFICA SE O CARTÃO PRESENTE NO LEITOR É DIFERENTE DO ÚLTIMO CARTÃO LIDO. CASO NÃO SEJA, FAZ
 
  /***INICIO BLOCO DE CÓDIGO RESPONSÁVEL POR GERAR A TAG mfrc522 LIDA***/
  String strID = "";
  for (byte i = 0; i < 4; i++) {
    strID +=
    (mfrc522.uid.uidByte[i] < 0x10 ? "0" : "") +
    String(mfrc522.uid.uidByte[i], HEX);
  }
  strID.toUpperCase();
  /***FIM DO BLOCO DE CÓDIGO RESPONSÁVEL POR GERAR A TAG mfrc522 LIDA***/
  
  Serial.println(strID); //IMPRIME NA SERIAL O UID DA TAG mfrc522

  digitalWrite(LED_AZUL, LOW);
  if(strID.length() > 0) {
    digitalWrite(LED_VERDE, HIGH);
    delay(1000);
    digitalWrite(LED_VERDE, LOW);
  }
  else {
    digitalWrite(LED_VERMELHO, HIGH);
    delay(1000);
    digitalWrite(LED_VERMELHO, LOW);
  }
  
  mfrc522.PICC_HaltA(); //PARADA DA LEITURA DO CARTÃO
  mfrc522.PCD_StopCrypto1(); //PARADA DA CRIPTOGRAFIA NO PCD
}
