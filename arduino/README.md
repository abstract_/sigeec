# Projeto do *Hardware*
Neste diretório se encontram os arquivos refrentes ao *hardware* do projeto, ou seja, as *Tags RFID* e o arduino UNO.

## Introdução
Um dos principais requisitos especificados para o *hardware* é que sejam dois terminais independes, um par ao atendente e outro para o caixa. Em um primeiro momento, para o protótipo, esses terminais serão compostos por:

* KIT RFID 522MHz;
* Laptop Acer Aspire 5.
